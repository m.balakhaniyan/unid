<?php

interface NotifierFactory
{

    public function createNotifier();
}
