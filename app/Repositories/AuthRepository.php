<?php

namespace App\Repositories;

interface AuthRepository
{

    public function get_otp($phone);

    public function login_with_otp($OTP);
}
