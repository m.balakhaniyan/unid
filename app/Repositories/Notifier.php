<?php

interface Notifier
{

    public function send();

}
