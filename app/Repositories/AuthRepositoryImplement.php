<?php

namespace App\Repositories;

use App\User;
use http\Exception\BadQueryStringException;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp;
use SMSNotifier;

class AuthRepositoryImplement implements AuthRepository
{
    protected const SECRET = 'uzGjYacX2qUPfa88IF13qDpK9YWjsCisjJ51Mq0a';

    public function get_otp($phone)
    {
        if (!User::where('phone', $phone)->count()) {
            throw new \Exception('doesnt exist', 400);
        }
        $user = User::where('phone', $phone);
        $password = 1234;
        $notify = new \SMSNotifierFactory($phone,$password);
        $notifier = $notify->createNotifier();
        $notifier->send();
        $user->password = Hash::make($password);
        if ($user->save()) {
            return $phone;
        } else {
            throw new \Exception('cant create it', 400);
        }

    }

    public function login_with_otp($user)
    {
        $http = new GuzzleHttp\Client;
        $response = $http->post('/oauth/token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => '2',
                'client_secret' => self::SECRET,
                'username' => $user->phone,
                'password' => $user->OTP,
                'scope' => '',
            ],
        ]);
        if ($response)
            return (string)$response->getBody();
        else
            return new \Exception('',400);
    }
}
