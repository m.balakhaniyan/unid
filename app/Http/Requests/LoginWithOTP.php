<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginWithOTP extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'OTP' => 'required|regex:/^[0-9a-zA-Z]{6,6}$/',
            'phone' => 'required|regex:/^09[0-9]{9,9}$/',
        ];
    }
}
