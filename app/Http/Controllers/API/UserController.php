<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Requests\GetOTP;
use App\Http\Requests\LoginWithOTP;
use App\Repositories\AuthRepository;
class UserController extends Controller
{
    public $successStatus = 200;
    /**
     * getOTP api
     *
     * @return \Illuminate\Http\Response
     */
    protected $user;
    public function __construct(AuthRepository $user)
    {
        $this->user = $user;
    }
    public function getOTP(GetOTP $req){

            return response()->json(['hi'=>$this->user->get_otp($req->phone)]);
    }
    public function loginWithOTP(LoginWithOTP $req){
           return response()->json(['hi'=>$this->user->login_with_otp($req)]);

    }
}
